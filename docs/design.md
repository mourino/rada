Design
======

These are some considerations taken into account when designing the client.


Colors
------

Do not rely on colors to convey information. I am not an expert in usability
nor accessibility, I don't know anything about color-blindness. Since I can't
ensure that the design will be accessible for color-blind people, I do not rely
in colors in any way to convey any kind of information. Colors are purely
decorative. This is not to say that color cannot be used to convey certain
information, but it will not be the only way, it will be accompanied by a bold
font or a visual cue.


Device support
--------------

The app works only in mobile devices, i.e. portrait mode. Designing and
developing a UI for mobile is a lot of work already, getting it to work for
tablets, laptops, and ultra-wide screens is way more work than any one single
person can tackle in a reasonable amount of time. This is out of scope until
the basic functionality is all there.

That said, if you are a designer and want to help bringing the project into
landscape modes I would be more than happy to work with you to make it happen.
Just keep in mind that even if we come up with the design, the implementation
can still take a while to begin. The goal now is to have a very solid, albeit
limited app with all the basic features working smoothly.
