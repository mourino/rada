Frequently Asked Questions
==========================

This is a short summary of the questions we anticipate that will arise when
looking into this project. You can read more in depth explanations in the
documentation of the project.
