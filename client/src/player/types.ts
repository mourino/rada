import type { Track } from "../graphql/api";

import { States } from "../queue/enums";

export interface IQueue {
  append: (t: Track[]) => void;
  clear: () => void;
  getCurrentTrack: () => Track | null;
  getDuration: () => number;
  getIndex: () => number;
  getTracks: () => Track[];
  getTrackAt: (i: number) => Track | null;
  setIndex: (i: number) => void;
  next: () => void;
  previous: () => void;
  removeAt: (i: number) => void;
}

export interface AudioEvent extends Omit<Event, "currentTarget"> {
  path?: HTMLAudioElement[];
  currentTarget: HTMLAudioElement;
}
export interface AudioEventListener extends EventListener {
  this: HTMLAudioElement;
  ev?: AudioEvent;
}

export interface IPlayer {
  audio: HTMLAudioElement;
  queue: IQueue;
  state: States;
  __currentTime: number;

  getCurrentTime: () => number;
  getCurrentTrack: () => Track | null;
  getIndex: () => number;
  setIndex: (i: number) => void;
  getQueue: () => Track[];
  getQueueLength: () => number;
  getAlbumCount: () => number;
  getTrackAt: (i: number) => Track | null;
  replaceQueue: (tt: Track[]) => void;
  appendTracks: (tt: Track[]) => void;
  removeTrackAt: (i: number) => void;
  removeAlbum: (i: number) => void;

  atFirstTrack: () => boolean;
  atLastTrack: () => boolean;
  isLoading: () => boolean;
  isPaused: () => boolean;
  isPlaying: () => boolean;

  pause: () => void;
  play: () => Promise<void>;
  stop: () => void;
  seekBy: (d: number) => void;
  seekTo: (p: number) => void;
  skipNext: () => void;
  skipPrevious: () => void;
  skipTo: (i: number) => void;
  togglePlayback: () => void;
  clearQueue: (options?: ClearQueueOptions) => void;
  stopAfter: (i: number) => void;
}

interface ClearQueueOptions {
  silent?: boolean;
}
