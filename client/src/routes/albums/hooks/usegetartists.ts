import { AlbumId } from "../../../types";

const useGetArtists = (id: AlbumId) => {
  return { artists: [] };
};

export default useGetArtists;
