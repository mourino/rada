import { h } from "preact";

const Notfound = () => (
  <div>
    <h1>Page not found</h1>
    <p>Sorry, we couldn&apos;t find the page you requested.</p>
  </div>
);

export default Notfound;
