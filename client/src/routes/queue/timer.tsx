import { h } from "preact";
import { useLayoutEffect, useRef } from "preact/hooks";

import toMinutes from "../../utils/tominutes";

interface TimerProps {
  current: number;
  total?: number | null;
  playing: boolean;
}

const Timer = ({ current, total, playing }: TimerProps) => {
  const ref = useRef<HTMLDivElement>(null);

  if (!total) return null;

  const render = (el: HTMLDivElement | null, time: number) => {
    if (el) el.innerText = toMinutes(time > total ? total : time);
  };

  let elapsed = current;
  useLayoutEffect(() => {
    render(ref.current, elapsed);
    if (!playing) return;

    const intervalId = setInterval(() => {
      render(ref.current, ++elapsed);
    }, 1000);

    return () => clearInterval(intervalId);
  }, [playing]);

  return <div ref={ref} />;
};

export default Timer;
