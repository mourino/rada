import { h, Fragment } from "preact";
import { useEffect, useRef, useState } from "preact/hooks";
import { Link } from "preact-router";

import type { IPlayer } from "../../player/types";

import Header from "../albums/header";
import Shoulder from "../../components/layout/shoulder";
import toMinutes from "../../utils/tominutes";
import { toHref } from "../../utils/id";
import { AlbumId } from "../../types";
import { Artist, Track } from "../../graphql/api";
import Logger from "../../logger";

import clearQueueIcon from "./clear.svg";
import style from "./style.css";
import playSmallIcon from "./play-small.svg";
import Timer from "./timer";
import Vinyl from "./vinyl";

const log = new Logger(__filename);
const HIDE_ANIMATION_DURATION = 250;
let wasVisible: boolean;

interface QueueProps {
  player: IPlayer;
  visible: boolean;
  onDismiss: () => void;
}

const Queue = ({ player, visible, onDismiss }: QueueProps) => {
  log.debug(`Queue.render()`);
  const { queue } = player;
  const ref = useRef<HTMLDivElement>(null);
  const [removeAnimation, setRemoveAnimation] = useState(true);

  const preventBodyScroll = () => document.body.classList.add(style.noscroll);
  const enableBodyScroll = () => document.body.classList.remove(style.noscroll);

  // In mobile, when you click on the search and the keyboard is displayed, the
  // viewport height changes. When you dismiss the keyboard the queue had adapted
  // to the new page height and it will show up where the keyboard used to be and
  // transition off the screen. That's why we need to remove the transition when
  // the queue is hidden.
  const handleVisibilityChange = () => {
    // Changes to be applied when the queue is not visible any more.
    const onQueueHidden = () => {
      setRemoveAnimation(true);
      ref.current?.scrollTo({ top: 0 });
    };

    if (visible) setRemoveAnimation(false);
    else setTimeout(onQueueHidden, HIDE_ANIMATION_DURATION);
  };

  // Handle visibility (sliding in and out)
  useEffect(() => {
    if (visible) preventBodyScroll();
    else enableBodyScroll();

    if (wasVisible !== visible) handleVisibilityChange();
    wasVisible = visible;

    return () => enableBodyScroll();
  }, [visible]);

  const trackClickHandler = (index: number) => player.skipTo(index);
  const clearQueue = (ev: Event) => {
    player.pause();
    ev.stopPropagation();
    hideAndClear();
  };
  const hideAndClear = () => {
    onDismiss();
    setTimeout(() => player.clearQueue(), HIDE_ANIMATION_DURATION);
  };
  const track = player.getCurrentTrack();
  if (!track) return null;

  log.debug(`Current index at ${player.getIndex()}`);
  log.debug(`Current track is "${track.title}"`);
  let prevTrack: Track | undefined;
  const getArtistName = (track?: Track): string | null =>
    (track?.artists || []).map((a) => a.name).join(", ");

  const renderTrackList = (track: Track, index: number) => {
    const artistName = track.album?.isVa ? "V/A" : getArtistName(track);
    const getAlbumName = (track: Track): string => {
      if (!track.album) return "<no album>";
      if (!track.album?.name) return "<no title>";
      let volume = "";
      if (track.album.volumes > 1) {
        volume = `(${track.volume}/${track.album.volumes})`;
      }

      return `${track.album.name} ${volume}`;
    };
    let artistChanged: boolean = true;
    let albumChanged: boolean = true;
    // If the first track in the list has no album the check
    // `track.album?.id !== prevTrack?.album?.id` will always be true.
    // To mitigate this we make an explicit check for `undefined` to
    // discriminate between "this is the first track" and "the previous track
    // has no album".
    if (prevTrack !== undefined) {
      artistChanged = artistName !== getArtistName(prevTrack);
      const volumeChange = track.volume !== prevTrack?.volume;
      albumChanged = volumeChange || track.album?.id !== prevTrack?.album?.id;
    }
    const isCurrentTrack = queue.getIndex() === index;
    const trackClasses = style.track + (isCurrentTrack ? ` ${style.current}` : "");
    const clickHandler = isCurrentTrack ? undefined : () => trackClickHandler(index);
    const rmTrack = (index: number): ((ev: Event) => void) => {
      return (ev: Event) => {
        ev.stopPropagation();
        // When we are removing the last track in the queue we treat it as a
        // clearQueue() instead, because it's the same for all practical purposes.
        if (player.getQueueLength() === 1) hideAndClear();
        else player.removeTrackAt(index);
      };
    };
    const rmAlbum = (startingAt: number) => {
      return (ev: Event) => {
        ev.stopPropagation();
        if (player.getAlbumCount() === 1) hideAndClear();
        else player.removeAlbum(startingAt);
      };
    };
    // If the album is V/A then we ignore the artist because it will obviously
    // be different for every track.
    const showHeader = track.album?.isVa ? albumChanged : artistChanged || albumChanged;

    const trackJsx = (
      <Fragment>
        {showHeader && (
          <div class={style.header}>
            <div>
              <span class={`${style.artists} ${artistName ? "" : style.missing}`}>
                {artistName || "<no artist>"}
              </span>
              &nbsp;
              <span class={`${style.album} ${track.album?.name ? "" : style.missing}`}>
                {getAlbumName(track)}
              </span>
            </div>
            <div class={style.rm} onClick={rmAlbum(index)}>
              ×
            </div>
          </div>
        )}
        <div key={`q-track-${index}`} class={trackClasses} onClick={clickHandler}>
          <div class={style.index}>
            {isCurrentTrack ? (
              player.isLoading() ? (
                <div class={style.hideIndex}>
                  {index + 1}.<div class={style.loader}>&nbsp;</div>
                </div>
              ) : player.isPlaying() ? (
                <div class={style.playing}>
                  <img src={playSmallIcon} />
                </div>
              ) : (
                `${index + 1}.`
              )
            ) : (
              `${index + 1}.`
            )}
          </div>
          <div class={style.title}>
            {track.title ? (
              track.title
            ) : (
              <span class={style.missing}>{"<no title>"}</span>
            )}
            <span class={style.artists}>
              {track.album?.isVa ? ` ${getArtistName(track)}` : ""}
              {track.features ? ` ft. ${(track.features || []).join(", ")}` : ""}
            </span>
            {track.info && <div class={style.info}>{track.info}</div>}
          </div>
          <div class={style.rm} onClick={rmTrack(index)}>
            &#215;
          </div>
        </div>
      </Fragment>
    );
    prevTrack = track;

    return trackJsx;
  };

  const qLength = queue.getTracks().length;
  const qDuration = queue.getDuration();

  const renderArtistLinks = (artists: Artist[], handler: EventListener) => {
    const artistJsx = artists.map((artist: Artist) => (
      <Link href={toHref(artist)} onClick={handler}>
        {artist.name}
      </Link>
    ));
    if (artistJsx.length < 2) return artistJsx;

    let commaSeparatedList: JSX.Element[] = [];
    for (let x = 0; x <= artistJsx.length; x++) {
      const artist = artistJsx[x];
      const isEven = x % 2 === 0;
      if (!isEven) {
        commaSeparatedList = commaSeparatedList.concat(<span>, </span>);
      }
      commaSeparatedList = commaSeparatedList.concat(artist);
    }

    return commaSeparatedList;
  };

  return (
    <Fragment>
      <div class={`${style.modal} ${visible ? style.visible : ""}`} />

      <section
        ref={ref}
        class={[
          style.queue,
          visible ? style.visible : style.hidden,
          removeAnimation ? style.noanim : "",
        ].join(" ")}
      >
        <Header
          key="queue-header"
          serverId={track.serverId}
          id={track.album?.id as AlbumId}
          hidePlayButton={true}
          hideNav={true}
          onClick={onDismiss}
        >
          <Vinyl
            onPlay={() => player.play()}
            onPause={() => player.pause()}
            isPlaying={player.isPlaying()}
            isLoading={player.isLoading()}
            totalTime={player.getCurrentTrack()?.lengthInSeconds || 0}
            currentTime={player.getCurrentTime()}
          />
        </Header>

        <Shoulder key="queue-shoulder" detail={true} noPadding={true}>
          <section class={style.timer}>
            <div class={style.remaining}>
              <Timer
                current={player?.getCurrentTime()}
                total={track.lengthInSeconds}
                playing={player?.isPlaying()}
              />
            </div>
            <div class={style.total}>{toMinutes(track.lengthInSeconds)}</div>
          </section>

          <section class={style.details}>
            <div class={style.title}>
              <Link href={toHref(track)} onClick={onDismiss}>
                {track.title ? (
                  track.title
                ) : (
                  <span class={style.missing}>{"<no title>"}</span>
                )}
              </Link>
            </div>
            <div class={style.artists}>
              {renderArtistLinks(track?.artists || [], onDismiss)}
            </div>
            <div class={style.album}>
              <Link href={toHref(track.album)} onClick={onDismiss}>
                {track.album?.name}
              </Link>
              &nbsp;
            </div>
          </section>

          <section class={style.tracklist}>
            <div class={style.heading}>
              <h2>
                Queue | {qLength} tracks ({toMinutes(qDuration)})
              </h2>
              <div onClick={clearQueue}>
                <img src={clearQueueIcon} />
              </div>
            </div>
            {queue.getTracks().map(renderTrackList)}
          </section>
        </Shoulder>
      </section>
    </Fragment>
  );
};

export default Queue;
