import { h, FunctionComponent } from "preact";
import { useEffect } from "preact/hooks";

import type { Server, ServerInvite } from "../../graphql/api";

import Logger from "../../logger";

import style from "./trash.css";
import trash from "./trash.svg";
import useDeleteInvite from "./hooks/usedeleteinvite";
import useDeleteServer from "./hooks/usedeleteserver";
import { is_invite_expired } from "./utils";

interface TrashServerProps {
  server: Server;
  invite?: never;
  onDelete: (server: Server) => void;
}

interface TrashInviteProps {
  server?: never;
  invite: ServerInvite;
  onDelete: (invite: ServerInvite) => void;
}

type TrashProps = TrashServerProps | TrashInviteProps;

const log = new Logger(__filename);
const cutId = (id?: string) => (id ? id.split("-")[0] : "");

const Trash: FunctionComponent<TrashProps> = (props) => {
  if (props.invite) {
    return <TrashInvite invite={props.invite} onDelete={props.onDelete} />;
  }
  if (props.server) {
    return <TrashServer server={props.server} onDelete={props.onDelete} />;
  }

  return <div />;
};

const TrashInvite: FunctionComponent<TrashInviteProps> = (props) => {
  const [deleteInvite, { loading, error, invite }] = useDeleteInvite();

  if (error) log.error(error);

  const handler = () => {
    if (loading) return;

    const status = is_invite_expired(props.invite) ? "expired" : "pending";
    const msg = `Delete ${status} invite "${cutId(props.invite.id)}"?`;
    if (confirm(msg)) deleteInvite(props.invite.id);
  };
  const classes = [style.trash, loading ? style.loading : ""];

  useEffect(() => {
    if (invite) props.onDelete(invite);
  }, [invite]);

  return <img src={trash} class={classes.join(" ")} onClick={handler} />;
};

const TrashServer: FunctionComponent<TrashServerProps> = (props) => {
  const [deleteServer, { loading, error, server }] = useDeleteServer();

  if (error) log.error(error);

  const handler = () => {
    if (loading) return;

    const msg = `Delete server "${props.server.name}" (${cutId(props.server.id)})?`;
    if (confirm(msg)) deleteServer(props.server.id);
  };
  const classes = [style.trash, loading ? style.loading : ""];

  useEffect(() => {
    if (server) props.onDelete(server);
  }, [server]);

  return <img src={trash} class={classes.join(" ")} onClick={handler} />;
};

export default Trash;
