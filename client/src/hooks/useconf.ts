import { useContext } from "preact/hooks";

import ConfContext from "../conf/context";

export default () => useContext(ConfContext);
