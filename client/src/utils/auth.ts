// import cognito from "@aws-cdk/aws-cognito";
import {
  CognitoUserPool,
  CognitoUser,
  AuthenticationDetails,
} from "amazon-cognito-identity-js";
import * as AWS from "aws-sdk/global";

import Logger from "../logger";
import type Config from "../config.json";

const log = new Logger(__filename);

interface AuthResponse {
  token: string;
  groups: string[];
}

const authenticate = (
  credentials: Credentials,
  config: typeof Config
): Promise<AuthResponse> => {
  log.debug(`Authenticating as ${credentials.username}`);

  const { username, password } = credentials;
  if (!username || !password) throw new Error("No user credentials provided");

  return new Promise((resolve, reject) => {
    const poolData = {
      UserPoolId: config.idp.userPoolId,
      ClientId: config.idp.clientId,
    };
    const pool = new CognitoUserPool(poolData);
    const authData = {
      Username: username,
      Password: password,
    };
    const userData = {
      Username: username,
      Pool: pool,
    };
    const auth = new AuthenticationDetails(authData);
    const user = new CognitoUser(userData);
    log.debug("Querying cognito...");
    user.setAuthenticationFlowType("USER_PASSWORD_AUTH");
    user.authenticateUser(auth, {
      onSuccess: (result) => {
        log.debug("Authentication successful");
        const loginData = {};
        const key = config.idp.url;
        (loginData as any)[key] = result.getIdToken().getJwtToken();
        AWS.config.update({ region: config.region });
        AWS.config.credentials = new AWS.CognitoIdentityCredentials({
          IdentityPoolId: config.idp.identityPoolId,
          Logins: loginData,
        });
        (AWS.config.credentials as any).refresh((error: Error) => {
          if (error) {
            log.warn(error);
            reject(error);
          } else {
            log.debug("Resolving...");
            resolve({
              token: result.getAccessToken().getJwtToken(),
              groups: result.getIdToken().payload["cognito:groups"] || [],
            });
          }
        });
      },
      onFailure: (err) => {
        log.warn(err);
        reject(err);
      },
    });
  });
};

const storeAccessToken = (token: string) => {
  return sessionStorage.setItem("awsAccessToken", token);
};

const getAccessToken = (): string => {
  const token = sessionStorage.getItem("awsAccessToken");
  if (!token) throw new Error("Unauthorized");
  return token;
};

interface Credentials {
  username: string;
  password: string;
}

const storeCredentials = (credentials: Credentials) => {
  const { username, password } = credentials;
  if (!username || !password) throw new Error("Must provide both username and password");
  log.warn(`localStorage.setItem("GawshiUsername", "${username}")`);
  localStorage.setItem("GawshiUsername", username);
  log.warn(`localStorage.setItem("GawshiPassword", "***********")`);
  localStorage.setItem("GawshiPassword", password);
};

const fetchCredentials = (): Credentials => {
  log.warn(`localStorage.getItem("GawshiUsername")`);
  const username = localStorage.getItem("GawshiUsername");
  log.warn(`localStorage.getItem("GawshiPassword")`);
  const password = localStorage.getItem("GawshiPassword");

  if (!username || !password) throw new Error("Credentials not found");

  return { username, password };
};

const clearCredentials = () => {
  log.warn(`localStorage.removeItem("GawshiUsername")`);
  localStorage.removeItem("GawshiUsername");
  log.warn(`localStorage.removeItem("GawshiPassword")`);
  localStorage.removeItem("GawshiPassword");
};

export {
  Credentials,
  authenticate,
  getAccessToken,
  storeAccessToken,
  clearCredentials,
  fetchCredentials,
  storeCredentials,
};
