export type AlbumId = string;
export type ArtistId = string;
export type ServerId = string;
export type TrackId = string;
