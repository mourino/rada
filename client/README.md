Gawshi Client
=============

CLI Commands
------------
*   `npm install`: Installs dependencies
*   `npm run dev`: Run a development, HMR server
*   `npm run serve`: Run a production-like server
*   `npm run build`: Production-ready build
*   `npm run lint`: Pass TypeScript files using ESLint
*   `npm run test`: Run Jest and Enzyme with
    [`enzyme-adapter-preact-pure`](https://github.com/preactjs/enzyme-adapter-preact-pure)
    for your tests

For detailed explanation on how things work, checkout the [CLI Readme](https://github.com/developit/preact-cli/blob/master/README.md).

Credits
-------
* Header background: https://www.piqsels.com/en/public-domain-photo-svyyj
* Vinyl icon: https://commons.wikimedia.org/wiki/File:LP_Vinyl_Symbol_Icon.png
* Vinyl spinner: https://www.deviantart.com/natleyn/art/Vinyl-Scratch-s-Animated-Cursor-Set-Finally-339539046
* Raleway font: https://github.com/impallari/Raleway
